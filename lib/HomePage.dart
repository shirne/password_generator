
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'PasswordGenerator.dart';
import 'generated/l10n.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  platformIsWindow(){
    if(kIsWeb){
      return false;
    }
    if(Platform.isWindows){
      return true;
    }
    if(Platform.isMacOS){
      return true;
    }
    if(Platform.isLinux){
      return true;
    }

    return false;
  }

  @override
  State<HomePage> createState() =>
    kIsWeb? _WebHomePageState():
      platformIsWindow() ? _WindowHomePageState() : _MobileHomePageState();
}

class _WindowHomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      body: Center(
        child: PasswordGenerator(isWindow: true),
      ),
    );
  }
}

class _WebHomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).password_generator),
      ),
      body: Center(
        child: PasswordGenerator(isWindow: false),
      ),
    );
  }
}

class _MobileHomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).password_generator),
      ),
      body: Center(
        child: PasswordGenerator(isWindow: false),
      ),
    );
  }
}