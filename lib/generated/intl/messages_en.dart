// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "alert": MessageLookupByLibrary.simpleMessage("Alert"),
        "character_repertoire_cannot_empty":
            MessageLookupByLibrary.simpleMessage(
                "Character repertoire can\'t be empty"),
        "clear": MessageLookupByLibrary.simpleMessage("Clear"),
        "contains_capital_letters":
            MessageLookupByLibrary.simpleMessage("Contains capital letters"),
        "contains_lowercase_letters":
            MessageLookupByLibrary.simpleMessage("Contains lowercase letters"),
        "contains_numbers":
            MessageLookupByLibrary.simpleMessage("Contains numbers"),
        "default_count": MessageLookupByLibrary.simpleMessage("Default %d"),
        "default_length": MessageLookupByLibrary.simpleMessage("Default %d"),
        "generate": MessageLookupByLibrary.simpleMessage("Generate"),
        "generate_count":
            MessageLookupByLibrary.simpleMessage("Count to generate"),
        "generator_count_greater": MessageLookupByLibrary.simpleMessage(
            "Generator count must greater then %d"),
        "lower_characters_a_z":
            MessageLookupByLibrary.simpleMessage("The characters a-z"),
        "numbers_0_9": MessageLookupByLibrary.simpleMessage("Numbers 0-9"),
        "ok": MessageLookupByLibrary.simpleMessage("OK"),
        "other_characters":
            MessageLookupByLibrary.simpleMessage("Other characters"),
        "other_chars_includes":
            MessageLookupByLibrary.simpleMessage("Other characters needed"),
        "password_generator":
            MessageLookupByLibrary.simpleMessage("Password Generator"),
        "password_length":
            MessageLookupByLibrary.simpleMessage("Password length"),
        "password_length_greater": MessageLookupByLibrary.simpleMessage(
            "Password length must greater then %d"),
        "uppercase_characters_a_z":
            MessageLookupByLibrary.simpleMessage("The characters A-Z")
      };
}
