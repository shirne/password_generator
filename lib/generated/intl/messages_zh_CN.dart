// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a zh_CN locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'zh_CN';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "alert": MessageLookupByLibrary.simpleMessage("温馨提示"),
        "character_repertoire_cannot_empty":
            MessageLookupByLibrary.simpleMessage("字符表不能为空"),
        "clear": MessageLookupByLibrary.simpleMessage("清除"),
        "contains_capital_letters":
            MessageLookupByLibrary.simpleMessage("包含大写字母"),
        "contains_lowercase_letters":
            MessageLookupByLibrary.simpleMessage("包含小写字母"),
        "contains_numbers": MessageLookupByLibrary.simpleMessage("包含数字"),
        "default_count": MessageLookupByLibrary.simpleMessage("默认%d条"),
        "default_length": MessageLookupByLibrary.simpleMessage("默认%d个字符"),
        "generate": MessageLookupByLibrary.simpleMessage("生成"),
        "generate_count": MessageLookupByLibrary.simpleMessage("生成的数量"),
        "generator_count_greater":
            MessageLookupByLibrary.simpleMessage("生成数量必须大于 %d"),
        "lower_characters_a_z":
            MessageLookupByLibrary.simpleMessage("26个小写字母 a-z"),
        "numbers_0_9": MessageLookupByLibrary.simpleMessage("数字 0-9"),
        "ok": MessageLookupByLibrary.simpleMessage("确定"),
        "other_characters": MessageLookupByLibrary.simpleMessage("其它字符"),
        "other_chars_includes":
            MessageLookupByLibrary.simpleMessage("需要包含的其它字符"),
        "password_generator": MessageLookupByLibrary.simpleMessage("密码生成器"),
        "password_length": MessageLookupByLibrary.simpleMessage("密码长度"),
        "password_length_greater":
            MessageLookupByLibrary.simpleMessage("密码长度必须大于 %d"),
        "uppercase_characters_a_z":
            MessageLookupByLibrary.simpleMessage("26个大写字母 A-Z")
      };
}
