// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Password Generator`
  String get password_generator {
    return Intl.message(
      'Password Generator',
      name: 'password_generator',
      desc: '',
      args: [],
    );
  }

  /// `Generate`
  String get generate {
    return Intl.message(
      'Generate',
      name: 'generate',
      desc: '',
      args: [],
    );
  }

  /// `Clear`
  String get clear {
    return Intl.message(
      'Clear',
      name: 'clear',
      desc: '',
      args: [],
    );
  }

  /// `Alert`
  String get alert {
    return Intl.message(
      'Alert',
      name: 'alert',
      desc: '',
      args: [],
    );
  }

  /// `OK`
  String get ok {
    return Intl.message(
      'OK',
      name: 'ok',
      desc: '',
      args: [],
    );
  }

  /// `Password length must greater then %d`
  String get password_length_greater {
    return Intl.message(
      'Password length must greater then %d',
      name: 'password_length_greater',
      desc: '',
      args: [],
    );
  }

  /// `Generator count must greater then %d`
  String get generator_count_greater {
    return Intl.message(
      'Generator count must greater then %d',
      name: 'generator_count_greater',
      desc: '',
      args: [],
    );
  }

  /// `Character repertoire can't be empty`
  String get character_repertoire_cannot_empty {
    return Intl.message(
      'Character repertoire can\'t be empty',
      name: 'character_repertoire_cannot_empty',
      desc: '',
      args: [],
    );
  }

  /// `Contains capital letters`
  String get contains_capital_letters {
    return Intl.message(
      'Contains capital letters',
      name: 'contains_capital_letters',
      desc: '',
      args: [],
    );
  }

  /// `Contains lowercase letters`
  String get contains_lowercase_letters {
    return Intl.message(
      'Contains lowercase letters',
      name: 'contains_lowercase_letters',
      desc: '',
      args: [],
    );
  }

  /// `Contains numbers`
  String get contains_numbers {
    return Intl.message(
      'Contains numbers',
      name: 'contains_numbers',
      desc: '',
      args: [],
    );
  }

  /// `The characters A-Z`
  String get uppercase_characters_a_z {
    return Intl.message(
      'The characters A-Z',
      name: 'uppercase_characters_a_z',
      desc: '',
      args: [],
    );
  }

  /// `The characters a-z`
  String get lower_characters_a_z {
    return Intl.message(
      'The characters a-z',
      name: 'lower_characters_a_z',
      desc: '',
      args: [],
    );
  }

  /// `Numbers 0-9`
  String get numbers_0_9 {
    return Intl.message(
      'Numbers 0-9',
      name: 'numbers_0_9',
      desc: '',
      args: [],
    );
  }

  /// `Other characters`
  String get other_characters {
    return Intl.message(
      'Other characters',
      name: 'other_characters',
      desc: '',
      args: [],
    );
  }

  /// `Other characters needed`
  String get other_chars_includes {
    return Intl.message(
      'Other characters needed',
      name: 'other_chars_includes',
      desc: '',
      args: [],
    );
  }

  /// `Password length`
  String get password_length {
    return Intl.message(
      'Password length',
      name: 'password_length',
      desc: '',
      args: [],
    );
  }

  /// `Default %d`
  String get default_length {
    return Intl.message(
      'Default %d',
      name: 'default_length',
      desc: '',
      args: [],
    );
  }

  /// `Count to generate`
  String get generate_count {
    return Intl.message(
      'Count to generate',
      name: 'generate_count',
      desc: '',
      args: [],
    );
  }

  /// `Default %d`
  String get default_count {
    return Intl.message(
      'Default %d',
      name: 'default_count',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'zh', countryCode: 'CN'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
